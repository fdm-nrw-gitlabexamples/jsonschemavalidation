# JSON Schema Validation Template

This is a simple template to demonstrate GitLab CI for data validation using the [jsonschema](https://github.com/Julian/jsonschema) command line tool for generating a [JUnit](https://www.ibm.com/docs/en/adfz/developer-for-zos/14.1.0?topic=formats-junit-xml-format) compatible output that can be parsed by GitLab.

This example script looks for all `*.json` files in a subdirectory of the repository and passes them together with a [JSON Schema](https://json-schema.org/) file to the validator.

## Quickstart

1. Create a fork of this repository or copy all the files to a new repository
1. Add a license of your choosing e.g. see https://choosealicense.com/non-software/
    * Update `LICENSE` file
1. Add your `.json` files
1. Update the variables in `.gitlab-ci.yml` so the script can find your `.json` files

On every push to the repository or in merge requests GitLab will parse the test results and show them accordingly. For Example:
* In pipelines like [here](https://gitlab.com/fdm-nrw-gitlabexamples/jsonschemavalidation/-/pipelines/351641176/test_report)
* In merge requeste like [here](https://gitlab.com/fdm-nrw-gitlabexamples/jsonschemavalidation/-/merge_requests/1)
  * GitLab will automatically update the test result when a commit is pushed
  * GitLab will also require you to click twice when merging a branch with a failed test
  * You can configure gitlab to prevent merging entirely while tests are in failed state

## How This Works in Detail

This repository uses the GitLabs [test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html) feature

## About JSON Schema

[JSON Schema](https://json-schema.org/) is a way to define the structure of a JSON document. You can create rules for
* Mandatory Properties
* Valid Numeric Ranges
* Types (`number`, `string`, `object`, `array`)
* String Regexes
* and more ...
There are some [learning resources on the spec page](https://json-schema.org/learn/getting-started-step-by-step.html) that explain how to write a JSON Schema.

## About jsonschema Command Line Tool

[jsonschema](https://github.com/Julian/jsonschema) is a validator for JSON Schemas written in python. In the `.gitlab-ci.yml` file a docker image [`freifunkhamm/jsonschema`](https://hub.docker.com/r/freifunkhamm/jsonschema) is used that has the validator already installed.

jsonschema can also be used as a python library if you plan to integrate it deeper in your workflow. See their [pypi page](https://pypi.org/project/jsonschema) for more details on how to use it.

## About JUnit XML

[JUnit XML](https://www.ibm.com/docs/en/adfz/developer-for-zos/14.1.0?topic=formats-junit-xml-format) format was originally designed for reporting test results in the Java programming language ecosystem. I became a de-facto standard for reporting test cases of other programming languages and most testing frameworks have converters or output formatters for this  format. You can find a JUnit XML Schema [here](https://github.com/windyroad/JUnit-Schema/blob/master/JUnit.xsd). GitLab offers to parse JUnit XML files to create graphical report e.g. in pipelines or in merge requests.

## Putting Things Together

In this example the output of jsonschema is manually converted to JUnit XML as part of the `.gitlab-ci.yml` script in the `validate` stage using a few lines of unix commands. This can also be used as an example of how to generate JUnit XML from arbitrary sources.

Below you can find a commented version of the script and variables used

Variables:
* `SCHEMA`: schema file used for validation will be resolved using `schemas/$SCHEMA.schema.json`. e.g. in this repo `"person"`
* `JSONDIR`: directory that holds the `.json` files that need to be validated. e.g. in this repo `"data"`
* `JSONNAME`: name or pattern to filter the files in the directory. e.g. in this repo `"*.json"`
* `FAILONERROR`: `1` if you want the CI job to fail (:x:) when a single `.json` file is not valid or `0` if you want it to succeed (:heavy_check_mark:)

```sh
# We will use `.out/$JSONDIR` as directory to store validation results. Make sure that it exists.
mkdir -p ".out/$JSONDIR"
# Add the XML header to the output file
echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" >> .out/report.xml
# Make an XML root element for all the tests
echo "<testsuites id=\"$(date -Iseconds)\" name=\"JSON Schema Validation\">" >> .out/report.xml
# Make a test "group" for all validations run agains our schema
# You can replicate this block (and the lines generating the actual report) to run multiple validations
echo "<testsuite id=\"jsonschema.$SCHEMA\" name=\"Product Schema Validation\">" >> .out/report.xml
FAIL=0
# Use find to list all files in `$JSONDIR`
# If you want to include subfolders you have to change the parameter `-maxdepth` accordingly
# You might want to add `mkdir -p $(dirname $REPORT)` to add subdirectories in the `.out` folder
for FILE in $(find "$JSONDIR" -maxdepth 1 -name "$JSONNAME");
do
    REPORT=".out/$FILE.report"
    # Perform the actual validation using jsonschema and write validation results to a file
    # using the propoesed config a file `data/file.json` will be reported in a file
    # called `.out/data/file.json.report`
    # If the validation failed record that but do not exit before all files were validated
    jsonschema -i "$FILE" "schemas/$SCHEMA.schema.json" 2>"$REPORT" || FAIL=1 ; (exit 0)
    # Add a line to the report that the validation was executed
    echo "<testcase id=\"$FILE@$SCHEMA\" name=\"Validation Report for $FILE\">" >> .out/report.xml
    # If the validation result contains an error (the file `$REPORT` not empty) then report the failure
    if [ -s "$REPORT" ]
    then
        # Reading failure from the result (`cat "$REPORT"`) and put that in the report.
        echo "<failure message=\"Validation Error\" type=\"ERROR\">$(cat "$REPORT")</failure>" >> .out/report.xml
    fi
    # Done with this validation
    echo "</testcase>" >> .out/report.xml
done
# End the test "group"
echo "</testsuite>" >> .out/report.xml
# Close the XML root, needs to be the very last in the file
echo "</testsuites>" >> .out/report.xml
# If the job is configured to fail on validation error and it did fail validation, then fail the job
(exit $(($FAILONERROR && $FAIL)))
```

## Extending for More Schemas or More Complex Directory Structures

If you want to validate multiple schemas or more complex directory structures in one git repository you have the following options:

1. Replicate the validate job for each directory and schema you would like to validate. Only change the `variables` section and the job name. Only recommended for few additional schemas / directories but the jobs can be run in parallel, this might speed up your pipeline. -OR-
1. Only replicate the lines indicated in the commented script that produce a `<testsuite>` within the report. And change the parameters for `JSONDIR` and `SCHEMA` on each run. With an additional `for` loop over the schemas / directories this can easily scale to hundreds of `<testsuite>`s. -OR-
1. The combination of both of the above

In the simple example the script is directly added to the `.gitlab-ci.yml` file. For more complex scenarios it is generally advisible to extract the script. This might also make the steps above a lot easier.
